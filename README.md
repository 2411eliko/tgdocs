# TgDocs

getting docs form https://core.telegram.org/bots/api

Requirements
* Python 3.6 or higher.
* BeautifulSoup

```python
from tgdocs import TgDocs


tg = TgDocs()

print(tg.search('send'))
print('sendMessage' in tg)
```

results
```python
>>> ['Sending files', 'sendMessage', 'sendPhoto', 'sendAudio', 'sendDocument', 'sendVideo',] etc..
>>> True
```

you can get the results in `html` of `markdown` format.

e.g.
```python
from tgdocs import TgDocs


tg = TgDocs(parse_mode='html')

print(tg.get_link('sendMessage'))
print(tg.get_description('Message'))
```
results:
```python
>>> <a href="https://core.telegram.org/bots/api#sendmessage">🔗</a>

>>> <strong>Message</strong>  <a href="https://core.telegram.org/bots/api#message">🔗</a>
>>> 
>>> This object represents a message.
>>> 
>>> parameters:
>>> <code>message_id</code>		<em>Integer</em>
>>> <code>from</code>		<em>User</em>
>>> <code>date</code>		<em>Integer</em>
>>> <code>chat</code>		<em>Chat</em>
>>> <code>forward_from</code>		<em>User</em>
>>> <code>forward_from_chat</code>		<em>Chat</em>
>>> <code>forward_from_message_id</code>		<em>Integer</em>
>>> <code>forward_signature</code>		<em>String</em>
>>> <code>forward_sender_name</code>		<em>String</em>
>>> <code>forward_date</code>		<em>Integer</em>
>>> <code>reply_to_message</code>		<em>Message</em>
>>> <code>edit_date</code>		<em>Integer</em>
>>> <code>media_group_id</code>		<em>String</em>
etc
```



