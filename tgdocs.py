from bs4 import BeautifulSoup
from urllib.request import urlopen


class TgDocs:
    _BASE = 'https://core.telegram.org'
    _BASE_API = _BASE + '/bots/api'

    def __init__(self, parse_mode: str = 'markdown'):
        """
        :param parse_mode: 'markdown' or 'html'
        """
        self._parse_mode(parse_mode)
        self._refresh()

    def search(self, key: str) -> list:
        """
        :param key: Search keyword
        :return: List of matching type and methods
        """
        return [
            method for method in self._docs_methods
            if key.lower() in method.lower()
        ]

    def get_description(self, key: str) -> str:
        """
        :param key: Search keyword
        :return: Description and parameters
        """
        if key not in self:
            return "Not found"

        key = key.lower().replace(' ', '-')

        for x in self.sup(['h4', 'h3']):
            if x.a.get('name') == key:
                description = x.find_next('p')
                title = self._BOLD_FORMAT(x.a.next_sibling)
                link = self.get_link(x.a.next_sibling)
                text = description.text
                break
        else:
            return "Not found"

        for i in x.next_siblings:
            if i.name == 'table':
                table = i.tbody('tr')
                break

            elif i.name in ('h4', 'h3'):
                table = ''
                break
        else:
            table = ''

        if description.a:
            for href in description('a'):
                text = text.replace(
                    href.text,
                    (
                        self._URL_BASE_FORMAT
                        if href['href'].startswith('#')
                        else self._URL_FORMAT
                    )(href.text, href['href'])
                )

        params = '\n'.join(
            f"{self._CODE_FORMAT(x('td')[0].text)}"
            f"\t\t"
            f"{self._ITALIC_FORMAT(x('td')[1].text)}"
            for x in table
        )
        if params:
            text += f'\n\nparameters:\n{params}'
        return f"{title}  {link}\n\n{text}"

    def get_link(self, key: str) -> str:
        """
        :param key: Search keyword
        :return: Format link
        """
        if key not in self:
            return 'Not found'

        url = f'{self._BASE_API}#{key.lower().replace(" ", "-")}'
        return self._URL_FORMAT(chr(128279), url)

    def _getting_docs_method(self):
        self._docs_methods = [
            a.next_sibling
            for a in self.sup.findAll("a", 'anchor')
        ]

    def _refresh(self):
        self.sup = BeautifulSoup(
            urlopen(self._BASE_API).read().decode(),
            "html.parser"
        )
        self._getting_docs_method()

    def _parse_mode(self, mode: str):
        if mode.lower() == 'markdown':
            self._URL_FORMAT = "[{}]({})".format
            self._URL_BASE_FORMAT = f"[{{}}]({self._BASE_API}{{}})".format
            self._BOLD_FORMAT = "*{}*".format
            self._ITALIC_FORMAT = "_{}_".format
            self._CODE_FORMAT = "```{}```".format
        else:
            self._URL_FORMAT = '<a href="{1}">{0}</a>'.format
            self._URL_BASE_FORMAT =\
                f'<a href="{self._BASE_API}{{1}}">{{0}}</a>'.format
            self._BOLD_FORMAT = "<strong>{}</strong>".format
            self._ITALIC_FORMAT = "<em>{}</em>".format
            self._CODE_FORMAT = "<code>{}</code>".format

    def __contains__(self, item: str):
        return item.lower() in (x.lower() for x in self._docs_methods)

