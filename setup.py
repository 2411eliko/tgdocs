from setuptools import setup

with open('requirements.txt', encoding='utf-8') as r:
    requires = [i.strip() for i in r]

with open('README.md', encoding='utf-8') as f:
    readme = f.read()

setup(
    name='tgdocs',
    version='0.0.1',
    description='tg docs for bot api',
    long_description=readme,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/2411eliko/tgdocs/blob/master/tgdocs.py',
    author='eliko2411',
    py_modules=['tgdocs'],
    scripts=['tgdocs.py'],
    install_requires=requires,
)
